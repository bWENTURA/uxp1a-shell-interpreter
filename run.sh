#!/usr/bin/env sh

if [ ! -d build ]
then
  echo "The build directory doesn't exist. Run build.sh";
  exit
fi
cd build || exit;
./main
cd ..