//
// Created by marta on 24.01.2020.
//

#ifndef AST_SCOPE_H
#define AST_SCOPE_H

#include <map>
#include <bits/unique_ptr.h>
#include "../shell/system_functions.h"

namespace ast {

    class scope {
    public:
        scope(shell::system_functions& system_fun);
        void set_symbol(const std::string& name, const std::string& value);
        std::string get_value(const std::string& name);
        shell::system_functions *const get_system_functions() const;
    private:
        std::map<std::string, std::string> symbol_table;
        shell::system_functions& systemFunctions;
    };
}

#endif //AST_SCOPE_H
