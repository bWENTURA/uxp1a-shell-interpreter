//
// Created by marta on 24.01.2020.
//

#ifndef AST_ARGUMENT_H
#define AST_ARGUMENT_H

#include <string>
#include <vector>
#include "scope.h"




namespace ast {

    class scope;

    class argument {
    public:
        argument() = default;

        argument(const argument &argument);

        argument(std::string value);

        argument& operator=(const argument &argument);
        bool operator==(const argument &argument);

        argument(argument &&argument) noexcept ;
        argument& operator=(argument &&argument);
        std::vector<std::string> evaluate(const ast::scope &scope);
        std::string get_value();
    private:
        std::string value;
    };
}


#endif //AST_ARGUMENT_H
