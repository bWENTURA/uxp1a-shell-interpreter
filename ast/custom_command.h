//
// Created by marta on 24.01.2020.
//

#ifndef AST_CUSTOM_COMMAND_H
#define AST_CUSTOM_COMMAND_H

#include <string>
#include <vector>
#include "scope.h"
#include "argument.h"
#include "command.h"

namespace ast {
    class custom_command: public command {
    public:
        explicit custom_command(std::string name, std::vector<unevaluated_argument>& arguments);
        int execute(scope& scope) override;
    private:
        std::string name;
    };
}


#endif //AST_CUSTOM_COMMAND_H
