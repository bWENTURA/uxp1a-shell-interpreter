//
// Created by marta on 25.01.2020.
//

#ifndef SHELLITO_ARGUMENT_FRAGMENT_H
#define SHELLITO_ARGUMENT_FRAGMENT_H

#include <string>
#include <utility>
#include "scope.h"

namespace ast {
    class argument_fragment {
    public:
        explicit argument_fragment(std::string  value) : value(std::move(value)) {}
        argument_fragment(const argument_fragment &argument_fragment){
            value = argument_fragment.value;
        }
        argument_fragment& operator=(const argument_fragment &other) {
            value = other.value;
        }
        argument_fragment(argument_fragment &&other) noexcept : value(other.value) {}
        argument_fragment&operator=(argument_fragment( &&other)) {
            value = other.value;
        }

        virtual std::string get_value(ast::scope& scope) {return value;};
    protected:
        std::string value;
    };
}


#endif //SHELLITO_ARGUMENT_FRAGMENT_H
