//
// Created by marta on 25.01.2020.
//

#ifndef SHELLITO_UNEVALUATED_ARGUMENT_H
#define SHELLITO_UNEVALUATED_ARGUMENT_H

#include <vector>
#include "argument_fragment.h"
#include "literal.h"
#include "variable.h"

namespace ast {
    class unevaluated_argument {
    public:
        unevaluated_argument() = default;
        explicit unevaluated_argument(std::vector<ast::argument_fragment*> argumentFragments);
        std::string evaluate(ast::scope&);
        void add_fragment(ast::argument_fragment* fragment);

    private:
        std::vector<ast::argument_fragment*> argument_fragments;
    };

}


#endif //SHELLITO_UNEVALUATED_ARGUMENT_H
