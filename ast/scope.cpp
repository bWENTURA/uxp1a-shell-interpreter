//
// Created by marta on 24.01.2020.
//

#include "scope.h"

#include <utility>

ast::scope::scope(shell::system_functions& systemFunctions) : systemFunctions(systemFunctions) {
    symbol_table = {
            {"?", "0"}
    };
}

void ast::scope::set_symbol(const std::string& name, const std::string& value) {
    if (symbol_table.count(name)) {
        symbol_table[name] = value;
    } else {
        symbol_table.insert({name, value});
    }
}

std::string ast::scope::get_value(const std::string& name) {
    return symbol_table.count(name)
                ? symbol_table[name]
                : "";
}

shell::system_functions *const ast::scope::get_system_functions() const {
    return &systemFunctions;
}

