//
// Created by marta on 24.01.2020.
//

#ifndef AST_CD_COMMAND_H
#define AST_CD_COMMAND_H

#include "command.h"

namespace  ast {
    class cd_command: public command {
    public:
        explicit cd_command(std::vector<unevaluated_argument> &arguments);
        int execute(scope& scope) override;
    };
}


#endif //AST_CD_COMMAND_H
