//
// Created by marta on 24.01.2020.
//

#ifndef AST_COMMAND_H
#define AST_COMMAND_H

#include <utility>
#include <vector>
#include "pipeable_node.h"
#include "argument.h"
#include "unevaluated_argument.h"

namespace ast {
    class command: public pipeable_node {
    public:
        explicit command(std::vector<unevaluated_argument>& arguments) : arguments(std::move(arguments)) {}
    protected:
        std::vector<unevaluated_argument> arguments;
    };
}

#endif //AST_COMMAND_H
