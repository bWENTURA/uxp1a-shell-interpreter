//
// Created by marta on 24.01.2020.
//

#include "redirect_input.h"
#include "unevaluated_argument.h"

ast::redirect_input::redirect_input(ast::pipeable_node &pn, ast::unevaluated_argument &fn):
redirect(pn, fn) {};

int ast::redirect_input::execute(ast::scope& scope) {
    int descIn, statVal;
    pid_t pid;

    pid = fork();
    if (!pid) {
        descIn = open(filename.evaluate(scope).c_str(), O_RDONLY);

        if (descIn == -1) { 
            perror("Error on Open of descIn");
            exit(EXIT_FAILURE);
            return -1;
        }

        if (dup2(descIn, 0) == -1) {
            perror("Cannot duplicate descIn into stdin");
            exit(EXIT_FAILURE);
            return -2;
        }

        if (close(descIn) == -1) {
            perror("Error on closing descIn");
            exit(EXIT_FAILURE);
            return -3;
        }

        if (!node.execute(scope)) {
            exit(EXIT_FAILURE);
            return -4;
        }
        exit(EXIT_SUCCESS);
    } 
    else if (pid < 0) {
        perror("Could not fork the process during creating input redirection");
        exit(EXIT_FAILURE);
        return -5;
    }
    else {
        do {
            waitpid(pid, &statVal, 0);
        } while (!WIFEXITED(statVal));
    }

    return !WEXITSTATUS(statVal);
}