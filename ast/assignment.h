//
// Created by marta on 24.01.2020.
//

#ifndef SHELLITO_ASSIGNMENT_H
#define SHELLITO_ASSIGNMENT_H

#include <utility>
#include <vector>

#include "node.h"
#include "argument.h"
#include "argument_fragment.h"
#include "unevaluated_argument.h"

namespace ast {
    class assignment : public node {
    public:
        assignment(std::string name, ast::unevaluated_argument fragments) : value(std::move(fragments)),
                                                                            name(std::move(name)) {};

        assignment(const assignment &other) :
                name(other.name), value(other.value) {};

        assignment(assignment &&other) noexcept :
                name(other.name), value(other.value) {};

        int execute(scope &scope) override = 0;

    protected:
        const std::string name;
        unevaluated_argument value;
    };
}

#endif //SHELLITO_ASSIGNMENT_H
