//
// Created by marta on 24.01.2020.
//

#include "redirect_output.h"

ast::redirect_output::redirect_output(ast::pipeable_node &pn, ast::unevaluated_argument &fn) : redirect(pn, fn) {}

int ast::redirect_output::execute(ast::scope &scope) {
    int descOut, statVal;
    pid_t pid;

    pid = fork();
    if (!pid) {
        descOut = open(filename.evaluate(scope).c_str(), O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if (descOut == -1) {
            perror("Error on Open of descOut");
            exit(EXIT_FAILURE);
            return -1;
        }
        if (dup2(descOut, 1) == -1) {
            perror("Cannot duplicate descOut into stdout");
            exit(EXIT_FAILURE);
            return -2;
        }
        if (!node.execute(scope)) {
            exit(EXIT_FAILURE);
            return -3;
        }
        if (close(descOut) == -1) {
            perror("Error on closing descOut");
            exit(EXIT_FAILURE);
            return -4;
        }
        exit(EXIT_SUCCESS);
    } 
    else if (pid < 0) {
        perror("Could not fork the process during creating input redirection");
        exit(EXIT_FAILURE);
        return -5;
    } 
    else {
        do {
            waitpid(pid, &statVal, 0);
        } while (!WIFEXITED(statVal));
    }

    return !WEXITSTATUS(statVal);
}