//
// Created by marta on 25.01.2020.
//

#ifndef SHELLITO_LITERAL_H
#define SHELLITO_LITERAL_H

#include <utility>

#include "argument_fragment.h"

namespace ast {
    class literal : public argument_fragment {
    public:
        explicit literal(std::string value) : argument_fragment(value) {};
        literal(const literal &variable) : argument_fragment(variable) {};
        literal(literal &&variable) noexcept : argument_fragment(variable) {};

        std::string get_value(ast::scope& scope) override;
    };
}


#endif //SHELLITO_LITERAL_H
