//
// Created by marta on 24.01.2020.
//

#ifndef SHELLITO_LOCAL_ASSIGNMENT_H
#define SHELLITO_LOCAL_ASSIGNMENT_H

#include "assignment.h"

#include <utility>

namespace ast {
    class local_assignment: public assignment {
    public:
        local_assignment(const local_assignment& other) : assignment(other) {};
        local_assignment(local_assignment&& other) noexcept : assignment(other) {};

        local_assignment(std::string name, ast::unevaluated_argument fragments) : assignment(std::move(name), std::move(fragments))  {};

        int execute(scope& scope) override;
    };
}


#endif //SHELLITO_LOCAL_ASSIGNMENT_H
