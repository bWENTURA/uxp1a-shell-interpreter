//
// Created by marta on 24.01.2020.
//

#include "argument.h"
#include "../shell/system_functions.h"
#include <utility>
#include <vector>
#include <regex>

ast::argument::argument(std::string  value) : value(std::move(value)) {}

std::vector<std::string> ast::argument::evaluate(const ast::scope &scope) {
  auto filenames = scope.get_system_functions()->get_filenames_in_directory();
  std::vector<std::string> good_file_names = std::vector<std::string>();

  std::string searched_value = "^";
  for ( int i = 0 ; i < this->value.length(); i++)
  {
    if((this->value)[i] == '*')
      searched_value+= ".*";
    else
      searched_value+= this->value[i];
  }
  searched_value += "$";
  const std::regex r(searched_value);
  std::smatch sm;


  for(auto const& file_name: filenames) {
    if(regex_search(file_name, sm, r))
    {
      good_file_names.push_back(file_name);
    }
  }

if (good_file_names.size() == 0)
  good_file_names.push_back(this->value);
return good_file_names;
}

ast::argument::argument(const ast::argument &argument): value(argument.value) {}

ast::argument& ast::argument::operator=(const argument &argument) {
    value = argument.value;
    return *this;
}

ast::argument::argument(ast::argument &&argument) noexcept : value(std::move(argument.value)) {}

ast::argument &ast::argument::operator=(ast::argument &&argument) {
    value = std::move(argument.value);
    return *this;
}

bool ast::argument::operator==(const ast::argument &argument) {
    return (value == argument.value);
}

std::string ast::argument::get_value() {
    return value;
}
