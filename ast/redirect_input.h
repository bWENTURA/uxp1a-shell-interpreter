//
// Created by marta on 24.01.2020.
//

#ifndef AST_REDIRECT_INPUT_H
#define AST_REDIRECT_INPUT_H

#include "redirect.h"
#include "pipeable_node.h"

namespace ast {
    class redirect_input: public redirect {
    public:
        explicit redirect_input(pipeable_node& pn, ast::unevaluated_argument& fn);
        int execute(scope& scope) override;
    };
}

#endif //AST_REDIRECT_INPUT_H
