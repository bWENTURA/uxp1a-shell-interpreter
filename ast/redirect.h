//
// Created by marta on 24.01.2020.
//

#ifndef AST_REDIRECT_H
#define AST_REDIRECT_H

#include "pipeable_node.h"
#include "unevaluated_argument.h"
#include <string>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <cstdlib>
#include <fcntl.h>

namespace ast {
    class redirect: public pipeable_node {
    public:
        explicit redirect(pipeable_node& pn, ast::unevaluated_argument& fn): node(pn), filename(fn) {};
    protected:
        pipeable_node& node;
        ast::unevaluated_argument filename;
    };
}

#endif //AST_REDIRECT_H
