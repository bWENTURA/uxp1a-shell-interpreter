//
// Created by marta on 24.01.2020.
//

#include <iostream>
#include "echo_command.h"

ast::echo_command::echo_command(std::vector<unevaluated_argument> &arguments): command(arguments) {}

int ast::echo_command::execute(ast::scope& scope) {
    for (auto& arg_pattern : arguments) {
        auto evaluated_argument = ast::argument(arg_pattern.evaluate(scope));
        auto evaluated_values = evaluated_argument.evaluate(scope);
        for (auto& s : evaluated_values) {
            std::cout << s << " ";
        }
        std::cout << std::endl;
    }
    return 0;
}