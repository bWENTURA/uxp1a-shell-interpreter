//
// Created by marta on 24.01.2020.
//

#ifndef AST_ECHO_COMMAND_H
#define AST_ECHO_COMMAND_H

#include "command.h"

namespace ast {
    class echo_command: public command {
    public:
        explicit echo_command(std::vector<unevaluated_argument> &arguments);
        int execute(scope& scope) override;
    };
}

#endif //AST_ECHO_COMMAND_H
