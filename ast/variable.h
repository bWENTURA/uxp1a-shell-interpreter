// Maja Jabłońska

#ifndef SHELLITO_VARIABLE_H
#define SHELLITO_VARIABLE_H

#include "argument.h"
#include "argument_fragment.h"

namespace ast {
    class variable: public argument_fragment {
    public:
        explicit variable(const std::string& value) : argument_fragment(value) {};
        variable(const variable &variable) : argument_fragment(variable) {};
        variable& operator=(const variable &variable);

        variable(variable &&variable) noexcept : argument_fragment(variable) {};
        variable& operator=(variable &&variable);

        std::string get_value(ast::scope& scope);

    };
}

#endif //SHELLITO_VARIABLE_H
