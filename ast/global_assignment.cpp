//
// Created by marta on 24.01.2020.
//

#include "global_assignment.h"

int ast::global_assignment::execute(ast::scope &scope) {
    auto string_value = value.evaluate(scope).c_str();
    auto name_value = name.c_str();
    return scope.get_system_functions()->set_env(name_value, string_value);
}
