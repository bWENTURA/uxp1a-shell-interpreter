// Maja Jabłońska

#include "variable.h"


ast::variable &ast::variable::operator=(const ast::variable &variable) {
    value = variable.value;
}


std::string ast::variable::get_value(ast::scope& scope) {
    // first, find locally
    auto local_value = scope.get_value(value);
    if (local_value.empty()) {
        char* val = scope.get_system_functions()->get_value_from_env(value.c_str());
        if (val) return std::string(val);
        return std::string();
    }
    return local_value;
}

ast::variable &ast::variable::operator=(ast::variable &&variable) {
    value = variable.value;
}
