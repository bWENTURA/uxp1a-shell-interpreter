//
// Created by marta on 24.01.2020.
//

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE scope_test

#include <boost/test/unit_test_suite.hpp>
#include <boost/test/test_tools.hpp>
#include <fakeit.hpp>
#include "../scope.h"
#include "../../shell/system_functions.h"


BOOST_AUTO_TEST_SUITE(test_scope)

fakeit::Mock<shell::system_functions> system_functions_mock;

BOOST_AUTO_TEST_CASE(exit_code_in_symbol_table) {
    // given
    ast::scope scope(system_functions_mock.get());
    // validate
    BOOST_CHECK(scope.get_value("?") == "0");
}

BOOST_AUTO_TEST_CASE(new_symbol) {
    // given
    ast::scope scope(system_functions_mock.get());

    // act
    scope.set_symbol("symbol", "value");

    // validate
    BOOST_CHECK(scope.get_value("symbol") == "value");
}

BOOST_AUTO_TEST_CASE(unexisting_symbol) {
    // given
    ast::scope scope(system_functions_mock.get());

    // validate
    BOOST_CHECK(scope.get_value("nonexisting_symbol") == "");
}


BOOST_AUTO_TEST_SUITE_END()