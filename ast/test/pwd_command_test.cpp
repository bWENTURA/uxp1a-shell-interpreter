//
// Created by marta on 25.01.2020.
//

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE pwd_command_test


#include <boost/test/unit_test_suite.hpp>
#include <boost/test/test_tools.hpp>
#include <fakeit.hpp>
#include "../scope.h"
#include "../../shell/system_functions.h"
#include "../pwd_command.h"


BOOST_AUTO_TEST_SUITE(test_pwd_command)

    fakeit::Mock<shell::system_functions> system_functions_mock;

    BOOST_AUTO_TEST_CASE(no_arguments) {
        // prepare
        auto arguments = std::vector<ast::unevaluated_argument>();
        auto pwd_command = ast::pwd_command(arguments);
        auto scope = ast::scope(system_functions_mock.get());
        fakeit::When(Method(system_functions_mock, get_current_directory)).Return("/home");

        // act
        pwd_command.execute(scope);

        // validate
        fakeit::Verify(Method(system_functions_mock, get_current_directory));
    }


BOOST_AUTO_TEST_SUITE_END()