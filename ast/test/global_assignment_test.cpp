//
// Created by marta on 25.01.2020.
//

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE global_assignment_test


#include <boost/test/unit_test_suite.hpp>
#include <boost/test/test_tools.hpp>
#include <fakeit.hpp>
#include "../scope.h"
#include "../../shell/system_functions.h"
#include "../global_assignment.h"
#include "../literal.h"
#include "../variable.h"


BOOST_AUTO_TEST_SUITE(test_global_assignment)


    BOOST_AUTO_TEST_CASE(test_case) {
        fakeit::Mock<shell::system_functions> system_functions_mock;
        fakeit::When(Method(system_functions_mock, set_env)).Return(0);
        system_functions_mock.get().set_env("variable", "value");
        fakeit::Verify(Method(system_functions_mock, set_env).Using("variable", "value"));

    }

//    BOOST_AUTO_TEST_CASE(literal_passed) {
//        // prepare
//        auto argument = ast::unevaluated_argument();
//        auto literal = ast::literal("value");
//        argument.add_fragment(&literal);
//        auto global_assignment = ast::global_assignment("variable", argument);
//        fakeit::Mock<shell::system_functions> system_functions_mock;
//        auto scope = ast::scope(system_functions_mock.get());
//        fakeit::When(Method(system_functions_mock, set_env)).Return(0);
//
//        // act
//        global_assignment.execute(scope);
//
//        // validate
//        fakeit::Verify(Method(system_functions_mock, set_env).Using("variable", "value"));
//    }



BOOST_AUTO_TEST_SUITE_END()