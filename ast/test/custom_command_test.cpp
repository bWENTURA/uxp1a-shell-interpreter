//
// Created by marta on 25.01.2020.
//


#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE custom_command


#include <boost/test/unit_test_suite.hpp>
#include <boost/test/test_tools.hpp>
#include <fakeit.hpp>
#include "../scope.h"
#include "../../shell/system_functions.h"
#include "../custom_command.h"


BOOST_AUTO_TEST_SUITE(test_custom_command)

    fakeit::Mock<shell::system_functions> system_functions_mock;

    BOOST_AUTO_TEST_CASE(no_arguments) {
        // given
        auto params = std::vector<ast::unevaluated_argument>();
        auto custom_command = ast::custom_command("function", params);
        fakeit::When(Method(system_functions_mock, execute_program)).Return(0);
        auto scope = ast::scope(system_functions_mock.get());

        // act
        custom_command.execute(scope);

        // validate
        fakeit::Verify(Method(system_functions_mock, execute_program));
    }

BOOST_AUTO_TEST_SUITE_END()