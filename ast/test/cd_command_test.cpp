//
// Created by bwentura on 26.01.2020.
//


#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE cd_command_test


#include <boost/test/unit_test_suite.hpp>
#include <boost/test/test_tools.hpp>
#include <fakeit.hpp>
#include "../scope.h"
#include "../../shell/system_functions.h"
#include "../cd_command.h"
#include "../literal.h"
#include "../argument_fragment.h"
#include "../unevaluated_argument.h"

BOOST_AUTO_TEST_SUITE(test_cd_command)

    fakeit::Mock<shell::system_functions> system_functions_mock;

    // BOOST_AUTO_TEST_CASE(no_arguments) {
    //     // given
    //     auto params = std::vector<ast::unevaluated_argument>();
    //     auto cd_command = ast::cd_command(params);
    //     fakeit::When(Method(system_functions_mock, change_directory)).Return(-1);
    //     auto scope = ast::scope(system_functions_mock.get());

    //     // act
    //     cd_command.execute(scope);

    //     // validate
    //     fakeit::Verify(Method(system_functions_mock, change_directory));
    // }

    // BOOST_AUTO_TEST_CASE(one_argument) {
    //     // given
    //     std::string string_cd1 = "test";
    //     ast::literal literal_cd1(string_cd1);
    //     std::vector<ast::argument_fragment*> arg_cd1 = {&literal_cd1};
    //     ast::unevaluated_argument uarg_cd1(arg_cd1);
    //     std::vector<ast::unevaluated_argument> params = {uarg_cd1};

    //     auto cd_command = ast::cd_command(params);
    //     fakeit::When(Method(system_functions_mock, change_directory)).Return(0);
    //     auto scope = ast::scope(system_functions_mock.get());

    //     // act
    //     cd_command.execute(scope);

    //     // validate
    //     fakeit::Verify(Method(system_functions_mock, change_directory));
    //     fakeit::Verify(Method(system_functions_mock, change_directory).Using("test"));
    // }

BOOST_AUTO_TEST_SUITE_END()