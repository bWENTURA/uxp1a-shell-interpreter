//
// Created by marta on 24.01.2020.
//

#include "cd_command.h"
#include "../shell/system_functions.h"

#include <utility>
#include <iostream>

ast::cd_command::cd_command(std::vector<unevaluated_argument> &arguments): command(arguments) {}

int ast::cd_command::execute(ast::scope &scope) {
    std::vector<char*> params;
    for (auto& arg_pattern : arguments) {
        auto evaluated_argument = ast::argument(arg_pattern.evaluate(scope));
        auto evaluated_values = evaluated_argument.get_value();

        params.push_back(const_cast<char*>(evaluated_values.c_str()));
    }
    if (params.empty() || params.size() > 1) {
        std::cout << "Wrong number of command arguments. There should be only one argument." << std::endl;
        return -1;
    }
    
    return scope.get_system_functions()->change_directory(params.at(0));
}