//
// Created by marta on 24.01.2020.
//

#ifndef AST_REDIRECT_OUTPUT_H
#define AST_REDIRECT_OUTPUT_H

#include "redirect.h"
#include "unevaluated_argument.h"

namespace ast {
    class redirect_output: public redirect {
    public:
        explicit redirect_output(pipeable_node& pn, ast::unevaluated_argument& fn);
        int execute(scope& scope) override;
    };
}

#endif //AST_REDIRECT_OUTPUT_H
