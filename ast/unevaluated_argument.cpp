//
// Created by marta on 25.01.2020.
//

#include "unevaluated_argument.h"

#include <utility>

ast::unevaluated_argument::unevaluated_argument(std::vector<ast::argument_fragment*> argumentFragments)
        : argument_fragments(argumentFragments) {}

std::string ast::unevaluated_argument::evaluate(ast::scope& scope) {
    std::string string_value = "";
    for (ast::argument_fragment* frag : argument_fragments) {
        string_value += frag->get_value(scope);
    }
    return string_value;
}

void ast::unevaluated_argument::add_fragment(ast::argument_fragment* fragment) {
    argument_fragments.push_back(fragment);
}
