//
// Created by marta on 24.01.2020.
//

#include "pwd_command.h"

#include <utility>
#include <iostream>

ast::pwd_command::pwd_command(std::vector<unevaluated_argument> &arguments) : command(arguments) {}

int ast::pwd_command::execute(ast::scope &scope) {
    auto directory = scope.get_system_functions()->get_current_directory();
    std::cout << directory << std::endl;
    return 0;
}
