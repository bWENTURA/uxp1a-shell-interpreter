//
// Created by marta on 24.01.2020.
//

#include "local_assignment.h"

#include <utility>

int ast::local_assignment::execute(ast::scope &scope) {
    scope.set_symbol(name, value.evaluate(scope));
    return 0;
}