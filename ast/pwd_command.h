//
// Created by marta on 24.01.2020.
//

#ifndef SHELLITO_PWD_COMMAND_H
#define SHELLITO_PWD_COMMAND_H

#include "command.h"

namespace ast {
    class pwd_command: public command {
    public:
        explicit pwd_command(std::vector<unevaluated_argument> &arguments);
        int execute(scope& scope) override;
    };

}


#endif //SHELLITO_PWD_COMMAND_H
