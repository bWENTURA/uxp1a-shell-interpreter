#include "pipe.h"


int ast::pipenode::execute(ast::scope& scope) {
    int pipefd[2], statVal;
    pid_t pid1, pid2;

    if (pipe(pipefd) == -1) {
        perror("Error when creating pipe");
        return -1;
    }

    pid1 = fork();
    if ( pid1 < 0 ) {
        perror("Error when creating fork");
        return -2;
    }

    if( pid1 == 0 ) {
        pid2 = fork();

        if ( pid2 < 0 ) {
            perror("Error when creating fork");
            return -2;
        }
        else if ( pid2 == 0 ) {
            if ( dup2(pipefd[0], 0) == -1 ) {
                perror("Error when dupping entry of pipe to entry of second process");
                exit(EXIT_FAILURE);
                return -3;
            }
            if ( close(pipefd[1]) == -1 ) {
                perror("Error closing out of pipe for second process");
                exit(EXIT_FAILURE);
                return -4;
            }
            if ( !node2.execute(scope) ) {
                exit(EXIT_FAILURE);
                return -5;
            }
            exit(EXIT_SUCCESS);
        }
        else {
            if ( dup2(pipefd[1], 1) == -1 ) {
                perror("Error when dupping out of pipe to entry of first process");
                exit(EXIT_FAILURE);
                return -3;
            }
            if ( close(pipefd[1]) == -1 ) {
                perror("Error closing entry of pipe for first process");
                exit(EXIT_FAILURE);
                return -4;
            }
            if ( !node1.execute(scope) ) {
                exit(EXIT_FAILURE);
                return -5;
            }
            exit(EXIT_SUCCESS);
        }
    }


    if (close(pipefd[0]) == -1) {
        perror("Error Closing Pipes for main process");
        return -6;
    }
    if (close(pipefd[1]) == -1) {
        perror("Error Closing Pipes for main process");
        return -6;
    }
    do {
        waitpid(pid1, &statVal, 0);
    } while (!WIFEXITED(statVal));

    return !WEXITSTATUS(statVal);
}