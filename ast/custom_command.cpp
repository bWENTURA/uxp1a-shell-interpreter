//
// Created by marta on 24.01.2020.
//

#include "custom_command.h"
#include "../shell/system_functions.h"
#include <iostream>

#include <utility>

int ast::custom_command::execute(ast::scope& scope) {
    // TODO: CHECK IF NOT EXIT
    // evaluate arguments into strings
    auto* params = new std::vector<char*>();
    params->push_back(const_cast<char*>(name.c_str()));
    for (auto& arg_pattern : arguments) {
        auto evaluated_argument = ast::argument(arg_pattern.evaluate(scope));
        auto evaluated_values = evaluated_argument.evaluate(scope);

        for (auto& s : evaluated_values) {
            auto str = new std::string(s);
            params->push_back(const_cast<char*>(str->c_str()));
        }
    }
    
    return scope.get_system_functions()->execute_program(params->at(0), *params);;
}

ast::custom_command::custom_command(std::string name, std::vector<unevaluated_argument>& arguments):
command(arguments), name(std::move(name)) {}
