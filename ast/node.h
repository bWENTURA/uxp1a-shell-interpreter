//
// Created by marta on 24.01.2020.
//

#ifndef AST_NODE_H
#define AST_NODE_H

#include "scope.h"

namespace ast {
    class node {
    public:
        virtual int execute(scope& scope) { return 0; }
    };
}

#endif //AST_NODE_H
