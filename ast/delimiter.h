//
// Created by marta on 24.01.2020.
//

#ifndef AST_DELIMITER_H
#define AST_DELIMITER_H

#include "node.h"
#include <memory>

namespace  ast {
    class delimiter {
    public:
        delimiter(node& left, node& right);
        int execute(scope& scope);
    private:
        node& left;
        node& right;
    };
}
#endif //AST_DELIMITER_H
