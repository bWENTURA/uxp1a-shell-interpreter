//
// Created by marta on 24.01.2020.
//

#include "delimiter.h"

ast::delimiter::delimiter(ast::node &left, ast::node &right):
left(left), right(right) {}

int ast::delimiter::execute(ast::scope &scope) {
    left.execute(scope);
    return right.execute(scope);
}