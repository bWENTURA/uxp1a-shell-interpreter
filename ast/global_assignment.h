//
// Created by marta on 24.01.2020.
//

#ifndef SHELLITO_GLOBAL_ASSIGNMENT_H
#define SHELLITO_GLOBAL_ASSIGNMENT_H

#include "assignment.h"

#include <utility>

namespace ast {
    class global_assignment: public assignment {
    public:
        explicit global_assignment(const std::string& name, unevaluated_argument arguments_) : assignment(name, std::move(arguments_)) {};
        global_assignment(const global_assignment& other) : assignment(other) {};
        global_assignment(global_assignment&& other) noexcept : assignment(other) {};
        int execute(scope& scope) override;
    };
}


#endif //SHELLITO_GLOBAL_ASSIGNMENT_H
