#ifndef AST_PIPE_H
#define AST_PIPE_H

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <cstdlib>
#include <fcntl.h>

#include "scope.h"
#include "pipeable_node.h"

namespace ast {
    class pipenode: public pipeable_node {
    public:
        explicit pipenode(pipeable_node& pn1, pipeable_node& pn2): node1(pn1), node2(pn2) {};
        int execute(scope& scope) override;
    protected:
        pipeable_node& node1;
        pipeable_node& node2;
    };
}

#endif //AST_PIPE_H
