//
// Created by marta on 24.01.2020.
//

#ifndef SHELLITO_SYSTEM_FUNCTIONS_H
#define SHELLITO_SYSTEM_FUNCTIONS_H

#include <string>
#include <vector>

namespace shell {
    class system_functions {
    public:
        virtual std::vector<std::string> get_filenames_in_directory();
        virtual int execute_program(const char* pathname, std::vector<char*>& args);
        virtual int set_env(const char* name, const char* value);
        virtual char* get_value_from_env(const char *name);
        virtual std::string get_current_directory();
        virtual int change_directory(const char* path);
    };

}

#endif //SHELLITO_SYSTEM_FUNCTIONS_H
