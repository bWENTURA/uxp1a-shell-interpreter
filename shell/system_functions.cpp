//
// Created by marta on 24.01.2020.
//

#include <vector>
#include <dirent.h>
#include "system_functions.h"
#include <unistd.h>
#include <sys/wait.h>
#include <unistd.h>
#include <iostream>

std::vector<std::string> shell::system_functions::get_filenames_in_directory() {
    std::vector<std::string> filenames;
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            filenames.emplace_back(dir->d_name);
        }
    }
    return filenames;
}

int shell::system_functions::execute_program(const char *pathname, std::vector<char*>& args) {
    pid_t pid = fork();
    int status;
    if (pid == 0) {
        status = execvp(pathname, args.data());
    } else if (pid < 0) {
        return -1;
    } else {
        do {
            waitpid(pid, &status, 0);
        } while (!WIFEXITED(status));
    }

    free(&args);
    return WEXITSTATUS(status);
}

int shell::system_functions::set_env(const char *name, const char* value) {
    return setenv(name, value, 1);
}

char *shell::system_functions::get_value_from_env(const char *name) {
    auto a = getenv(name);
    return a;
}

std::string shell::system_functions::get_current_directory() {
    char cwd[1024];
    getcwd(cwd, sizeof(cwd));
    return std::string(cwd);
}

int shell::system_functions::change_directory(const char* path) {
    int ret = chdir(path);
    if (errno != 0) {
        perror("");
        errno = 0;
    }
    return ret;
}


