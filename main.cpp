#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <scope.h>
#include <literal.h>
#include <pipe.h>
#include <system_functions.h>
#include <parser.h>
#include <node.h>

int main() {
    /*
    std::string a = "echo";
    std::string echostr = "abcd\nbsed";
    ast::literal arg(echostr);
    std::vector<ast::argument_fragment*> echoargs = {&arg};
    ast::unevaluated_argument uechoarg(echoargs);
    std::vector<ast::unevaluated_argument> uechoargs = {uechoarg};

    std::string grepstr = "ab";
    ast::literal arg2(grepstr);
    std::vector<ast::argument_fragment*> grepargs = {&arg2};
    ast::unevaluated_argument ugreparg(grepargs);
    std::vector<ast::unevaluated_argument> ugrepargs = {ugreparg};

    std::vector<ast::unevaluated_argument> ucatargs = {};

    ast::custom_command comm1("echo", uechoargs);
    ast::custom_command comm2("grep", ugrepargs);
    ast::custom_command comm3("cat", ucatargs);

    std::string fn1 = "test.txt";
    std::string fn2 = "test2.txt";

    auto arg1 = ast::unevaluated_argument();
    auto arg3 = ast::unevaluated_argument();
    auto lit1 = ast::literal(fn1);
    auto lit3 = ast::literal(fn2);
    arg1.add_fragment(lit1);
    arg3.add_fragment(lit3);
    ast::pipenode pnode(comm1, comm2);
    ast::redirect_output ronode(comm1, arg1);
    ast::redirect_input rinode(comm3, arg3);

    shell::system_functions sf;
    ast::scope sc(sf);
    pnode.execute(sc);
    ronode.execute(sc);
    rinode.execute(sc);*/
    std::stringstream sstr;
    std::string cmd;
    parser::parser p(sstr);
    shell::system_functions sf;
    ast::scope sc(sf);
    int returncode = 0;

    std::cout << ">> ";
    while(std::getline(std::cin, cmd)) {
        sstr << cmd;
        p.parse();
        ast::node* n = p.get_node();
        while (n != nullptr) {
            returncode = n->execute(sc); 
            n = p.get_node();
        }
        if(returncode) sc.set_symbol("?", std::to_string(returncode));
        returncode = 0;
        sstr.str(std::string());
        std::cout << ">> ";
    }

    return 0;
}
