cmake_minimum_required(VERSION 3.10)
project(shellito)
enable_testing()
set(CMAKE_CXX_STANDARD 17)

add_subdirectory(ast)
add_subdirectory(shell)
add_subdirectory(parser)


add_library(combinedlib INTERFACE)
target_link_libraries(combinedlib INTERFACE
    ast
    shell)

include_directories(ast shell parser)

add_executable(main main.cpp)
target_link_libraries(main 
    combinedlib
    parser)