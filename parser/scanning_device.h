// Maja Jabłońska

#ifndef PARSER_SCANNING_DEVICE_H
#define PARSER_SCANNING_DEVICE_H

#include "token_type.h"

namespace parser {
    class scanning_device {
    public:
        [[nodiscard]] virtual token_type peek_token_type(const int steps) const = 0;
        [[nodiscard]] virtual const std::string& peek_token_value(const int steps) const = 0;

        virtual void get(int n) = 0;

        virtual const int token_queue_size() = 0;

        virtual void scan() = 0;
    };
}

#endif //PARSER_SCANNING_DEVICE_H
