// Maja Jabłońska

#include "token.h"

parser::token::token(const std::string& v, const token_type t): value(v), _type(t) {}

parser::token_type parser::token::get_type() const {
    return _type;
}

const std::string &parser::token::get_value() const {
    return value;
}