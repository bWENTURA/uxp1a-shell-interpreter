// Maja Jabłońska

#ifndef UXP1A_SHELL_INTERPRETER_SCANNER_H
#define UXP1A_SHELL_INTERPRETER_SCANNER_H

#include <memory>
#include <string>
#include <vector>
#include <deque>

#include "scanning_device.h"
#include "token.h"

namespace parser {
    class stream_scanner: public scanning_device {
    public:
        explicit stream_scanner(std::istream& is);

        stream_scanner(const stream_scanner& other);
        stream_scanner(stream_scanner&& other) noexcept ;

        [[nodiscard]] token_type peek_token_type(const int steps) const override;
        [[nodiscard]] const std::string& peek_token_value(const int steps) const override;

        void get(int n) override;
        const int token_queue_size() override;
        void scan() override;

    private:
        // Read
        bool read_string();
        bool read_quoted_string();
        bool read_scoped_variable();
        bool read_variable();
        bool read_operator();

        // Helper methods
        void skip_whitespaces();

        // Checking token type
        static bool is_builtin_command(const std::string& value_);
        bool next_operator();
        bool next_allowed_in_string();
        bool next_allowed_in_var_name();
        bool next_quote();
        bool next_eof();
        bool next_whitespace();
        bool next_variable_call_symbol();
        bool next_variable_call_symbol_open();
        bool next_variable_call_symbol_close();

        [[nodiscard]] char get_char() const;
        void skip_char();

        std::istream& istream_;
        std::deque<std::unique_ptr<token>> tokens;
    };
}

#endif //UXP1A_SHELL_INTERPRETER_SCANNER_H