// Maja Jabłońska

#include <variable.h>
#include <literal.h>
#include "parser.h"

parser::parser::parser(std::istream &istream_):
sc(std::move(std::unique_ptr<scanning_device>(new stream_scanner(istream_)))) {}

parser::parser::parser(std::unique_ptr<scanning_device> sc): sc(std::move(sc)) {}

void parser::parser::parse() {
    sc->scan();
    while (sc->token_queue_size()) {
        if (parse_export_assignment()) continue;
        if (parse_assignment()) continue;
        if (parse_redirect_in()) continue;
        if (parse_redirect_out()) continue;
        if (parse_pipe()) continue;
        auto cmd = parse_command();
        if (cmd != nullptr) nodes.emplace_back(cmd);
        // If there is a remaining delimiter, get it
        if (sc->token_queue_size() && sc->peek_token_type(0) == token_type::DELIMITER) sc->get(1);
    }
}

bool parser::parser::parse_export_assignment() {
    if (!check_export_assignment()) return false;

    auto name = sc->peek_token_value(1);
    sc->get(3); // get export, var name and assignment

    auto arg = parse_argument();
    nodes.emplace_back(new ast::global_assignment(name, *arg));
    return true;
}

bool parser::parser::parse_redirect_in() {
    if (!check_redirect_in()) return false;
    if (nodes.empty()) return false;

    sc->get(1); // Get the '<' symbol

    auto arg = parse_argument();
    auto last_node = nodes.back();
    auto pipeable_node = dynamic_cast<ast::pipeable_node*>(last_node);
    nodes.pop_back();

    auto node = new ast::redirect_input(*pipeable_node, *arg);
    nodes.emplace_back(node);
    return true;
}

bool parser::parser::parse_redirect_out() {
    if (!check_redirect_out()) return false;
    if (nodes.empty()) return false;

    sc->get(1); // Get the '>' symbol

    auto arg = parse_argument();
    auto last_node = nodes.back();
    auto pipeable_node = dynamic_cast<ast::pipeable_node*>(last_node);
    nodes.pop_back();

    auto node = new ast::redirect_output(*pipeable_node, *arg);
    nodes.emplace_back(node);
    return true;
}

bool parser::parser::parse_pipe() {
    if (!check_pipe()) return false;
    if (nodes.empty()) return false;

    sc->get(1); // Get the '|' symbol

    auto last_node = nodes.back();
    auto pipeable_node = dynamic_cast<ast::pipeable_node*>(last_node);
    nodes.pop_back();
    auto command = parse_command();

    auto node = new ast::pipenode(*pipeable_node, *command);
    nodes.emplace_back(node);
    return true;
}

bool parser::parser::parse_assignment() {
    if (!check_assignment()) return false;
    auto name = sc->peek_token_value(0);
    sc->get(2); // get var name and assignment

    auto arg = parse_argument();
    nodes.emplace_back(new ast::local_assignment(name, *arg));
    return true;
}

ast::command* parser::parser::parse_command() {
    if (command_end()) return nullptr;

    auto cmd_type = sc->peek_token_type(0);
    auto name = sc->peek_token_value(0);
    sc->get(1);

    auto args = parse_arguments();
    switch (cmd_type) {
        case token_type::ECHO:
            return new ast::echo_command(args);
        case token_type::PWD:
            return new ast::pwd_command(args);
        case token_type::CD:
            return new ast::cd_command(args);
        default:
            return new ast::custom_command(name, args);
    }
}

bool parser::parser::check_export_assignment() {
    if (sc->token_queue_size() < 4) return false;

    // assignment = string, assign, (string | quoted string)
    return (sc->peek_token_type(0) == token_type::EXPORT &&
            sc->peek_token_type(1) == token_type::STRING &&
            sc->peek_token_type(2) == token_type::ASSIGN &&
                (sc->peek_token_type(3) == token_type::STRING ||
                 sc->peek_token_type(3) == token_type::QUOTED_STRING ||
                 sc->peek_token_type(3) == token_type::VAR ||
                 sc->peek_token_type(3) == token_type::ESCAPED_VAR));
}

bool parser::parser::check_assignment() {
    if (sc->token_queue_size() < 3) return false;

    // assignment = string, assign, (string | quoted string)
    return (sc->peek_token_type(0) == token_type::STRING &&
            sc->peek_token_type(1) == token_type::ASSIGN &&
                (sc->peek_token_type(2) == token_type::STRING ||
                 sc->peek_token_type(2) == token_type::QUOTED_STRING ||
                 sc->peek_token_type(2) == token_type::VAR ||
                 sc->peek_token_type(2) == token_type::ESCAPED_VAR));
}

bool parser::parser::check_redirect_in() {
    if (sc->token_queue_size() < 2) return false;
    if (nodes.empty()) return false;

    auto pipeable_node = dynamic_cast<ast::pipeable_node*>(nodes.back());
    if (pipeable_node == nullptr) return false;

    return (sc->peek_token_type(0) == token_type::REDIRECT_IN &&
            (sc->peek_token_type(1) == token_type::STRING ||
             sc->peek_token_type(1) == token_type::QUOTED_STRING ||
             sc->peek_token_type(1) == token_type::VAR ||
             sc->peek_token_type(1) == token_type::ESCAPED_VAR));
}

bool parser::parser::check_redirect_out() {
    if (sc->token_queue_size() < 2) return false;
    if (nodes.empty()) return false;

    auto pipeable_node = dynamic_cast<ast::pipeable_node*>(nodes.back());
    if (pipeable_node == nullptr) return false;

    return (sc->peek_token_type(0) == token_type::REDIRECT_OUT &&
            (sc->peek_token_type(1) == token_type::STRING ||
             sc->peek_token_type(1) == token_type::QUOTED_STRING ||
             sc->peek_token_type(1) == token_type::VAR ||
             sc->peek_token_type(1) == token_type::ESCAPED_VAR));
}

bool parser::parser::check_pipe() {
    if (sc->token_queue_size() < 2) return false;
    if (nodes.empty()) return false;

    auto pipeable_node = dynamic_cast<ast::pipeable_node *>(nodes.back());
    if (pipeable_node == nullptr) return false;

    return (sc->peek_token_type(0) == token_type::PIPE);
}

ast::node* parser::parser::get_node() {
    if (nodes.empty()) return nullptr;
    auto node = nodes.front();
    nodes.pop_front();
    return node;
}

ast::unevaluated_argument* parser::parser::parse_argument() {

    auto arg = new ast::unevaluated_argument();

    bool can_conc_with_next = false;
    do {
        auto type = sc->peek_token_type(0);
        if (type == token_type::ESCAPED_VAR) {
            auto var = new ast::variable(sc->peek_token_value(0));
            sc->get(1);
            arg->add_fragment(var);
            can_conc_with_next = can_conc_with_escaped_var();
        } else if (type == token_type::VAR) {
            auto var = new ast::variable(sc->peek_token_value(0));
            sc->get(1);
            arg->add_fragment(var);
            can_conc_with_next = can_conc_with_var();
        } else if (type== token_type::STRING || type == token_type::QUOTED_STRING) {
            auto literal = new ast::literal(sc->peek_token_value(0));
            sc->get(1);
            arg->add_fragment(literal);
            can_conc_with_next = can_conc_with_string();
        } else return nullptr;
    } while (can_conc_with_next);

    return arg;
}

std::vector<ast::unevaluated_argument> parser::parser::parse_arguments() {
    auto args = std::vector<ast::unevaluated_argument>();

    while (!command_end()) {
        auto arg = parse_argument();
        if (arg == nullptr) return args;
        args.emplace_back(*arg);
    }

    return args;
}

const bool parser::parser::command_end() {
    return !sc->token_queue_size() || sc->peek_token_type(0) == token_type::DELIMITER;
}

// Escaped var can be concatenated with string, quoted string, var call and escaped var call
const bool parser::parser::can_conc_with_escaped_var() {
    if (command_end()) return false;
    auto type = sc->peek_token_type(0);
    return (type == token_type::ESCAPED_VAR || type == token_type::VAR ||
            type == token_type::STRING || type == token_type::QUOTED_STRING);
}

const bool parser::parser::can_conc_with_var() {
    if (command_end()) return false;
    auto type = sc->peek_token_type(0);
    if (sc->peek_token_value(0).empty()) return false;
    auto first_letter = sc->peek_token_value(0).front();
    return (type == token_type::ESCAPED_VAR || type == token_type::VAR ||
            (type == token_type::STRING && (!isalnum(first_letter) && (first_letter != '_'))) ||
            type == token_type::QUOTED_STRING);
}

// Only escaped variable can be concatenated with string, quoted string or variable call
const bool parser::parser::can_conc_with_string() {
    if (command_end()) return false;
    return sc->peek_token_type(0) == token_type::ESCAPED_VAR || sc->peek_token_type(0) == token_type::VAR;
}
