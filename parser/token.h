// Maja Jabłońska

#ifndef PARSER_TOKEN_H
#define PARSER_TOKEN_H

#include <string>
#include "token_type.h"

namespace parser {
    class token {
    public:
        explicit token(const std::string& v, token_type t);

        [[nodiscard]] token_type get_type() const;
        [[nodiscard]] const std::string& get_value() const;
    private:
        const token_type _type;
        const std::string value;
    };
}

#endif //PARSER_TOKEN_H
