// Maja Jabłońska

#ifndef PARSER_TOKEN_TYPE_H
#define PARSER_TOKEN_TYPE_H

#include <map>

namespace parser {

    const char QUOTE = '\'';

    enum class token_type {
        ECHO,
        PWD,
        CD,
        EXPORT,
        VAR,
        ESCAPED_VAR,
        STRING,
        QUOTED_STRING,
        VAR_CALL_SYMBOL = '$',
        VAR_CALL_OPEN = '{',
        VAR_CALL_CLOSE = '}',
        PIPE = '|',
        ASSIGN = '=',
        REDIRECT_IN = '<',
        REDIRECT_OUT = '>',
        DELIMITER = ';',
    };

    const token_type operators[] = {
            token_type::PIPE, token_type::ASSIGN,
            token_type::REDIRECT_IN, token_type::REDIRECT_OUT, token_type::DELIMITER
    };

    inline bool is_operator(const char c) {
        for (const auto& op : operators) {
            if (static_cast<const char>(op) == c) return true;
        }
        return false;
    }

    const std::map<const std::string, const token_type> str_to_builtin_command = {
            {"echo", token_type::ECHO},
            {"pwd", token_type::PWD},
            {"cd", token_type::CD},
            {"export", token_type::EXPORT}
    };
}

#endif //PARSER_TOKEN_TYPE_H
