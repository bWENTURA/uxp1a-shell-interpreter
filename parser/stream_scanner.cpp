// Maja Jabłońska

#include "stream_scanner.h"
#include <memory>
#include <iostream>
#include <algorithm>
#include <string>

parser::stream_scanner::stream_scanner(std::istream &is): istream_(is) {}

parser::stream_scanner::stream_scanner(const parser::stream_scanner &other): istream_(other.istream_) {
    tokens = std::deque<std::unique_ptr<token>>(other.tokens.size());
    for (const auto& t : other.tokens) {
        tokens.push_back(std::move(std::make_unique<token>(*t)));
    }
}

parser::stream_scanner::stream_scanner(parser::stream_scanner &&other) noexcept: istream_(other.istream_) {
    tokens = std::deque<std::unique_ptr<token>>(other.tokens.size());
    for (auto& t : other.tokens) {
        tokens.push_back(std::move(t));
    }
}

parser::token_type parser::stream_scanner::peek_token_type(const int steps) const {
    if (steps < tokens.size() && steps >= 0)
        return tokens.at(steps)->get_type();
    else throw std::runtime_error("Provided steps is not in token size bounds, which is 0 to " + std::to_string(tokens.size()-1));
}

const std::string &parser::stream_scanner::peek_token_value(const int steps) const {
    if (steps < tokens.size() && steps >= 0)
        return tokens.at(steps)->get_value();
    else throw std::runtime_error("Provided steps is not in token size bounds, which is 0 to " + std::to_string(tokens.size()-1));
}

void parser::stream_scanner::get(int n) {
    if (n <= tokens.size() && n >= 0)
        for (int i = 0; i < n; ++i) tokens.pop_front();
    else throw std::runtime_error("Provided steps is not in token size bounds, which is 0 to " + std::to_string(tokens.size()-1));
}

const int parser::stream_scanner::token_queue_size() {
    return tokens.size();
}

void parser::stream_scanner::scan() {
    while (!next_eof()) {
        skip_whitespaces();
        if (read_operator()) continue;
        if (read_variable()) continue;
        if (read_quoted_string()) continue;
        if (read_string()) continue;
    }
    istream_.clear();
}

bool parser::stream_scanner::read_string() {
    std::string result;

    while (next_allowed_in_string())
        result += get_char();

    if (result.empty()) return false;

    if (is_builtin_command(result))
        tokens.push_back(std::move(std::make_unique<token>(result, str_to_builtin_command.at(result))));
    else
        tokens.push_back(std::move(std::make_unique<token>(result, parser::token_type::STRING)));
    return true;
}

bool parser::stream_scanner::is_builtin_command(const std::string &value_) {
   return str_to_builtin_command.count(value_);
}

bool parser::stream_scanner::read_quoted_string() {
    std::string result;
    if (next_quote()) {
        skip_char();
        while (istream_ && !next_quote()) {
            result += get_char();
        }
        skip_char();
        tokens.push_back(std::move(std::make_unique<token>(result, parser::token_type::QUOTED_STRING)));
        return true;
    }
    return false;
}

bool parser::stream_scanner::read_scoped_variable() {
    std::string result;
    if (!next_variable_call_symbol_open()) return false;
    skip_char();
    while (istream_ && !next_variable_call_symbol_close())
        result += get_char();
    if (next_variable_call_symbol_close()) skip_char();
    if (result.empty()) return false;
    tokens.push_back(std::move(std::make_unique<token>(result, token_type::ESCAPED_VAR)));
    return true;
}

bool parser::stream_scanner::read_variable() {
    std::string result;
    if (!next_variable_call_symbol()) return false;
    skip_char();
    if (read_scoped_variable()) return true;
    while (next_allowed_in_var_name())
        result += get_char();
    if (result.empty()) return false;
    tokens.push_back(std::move(std::make_unique<token>(result, token_type::VAR)));
    return true;
}

bool parser::stream_scanner::read_operator() {
    std::string result;
    if (next_operator()) {
        auto c = get_char();
        result += c;
        tokens.push_back(std::move(std::make_unique<token>(result, static_cast<token_type>(c))));
        return true;
    }
    return false;
}

void parser::stream_scanner::skip_whitespaces() {
    while (next_whitespace()) istream_.get();
}

bool parser::stream_scanner::next_operator() {
    return istream_ && is_operator(istream_.peek());
}

bool parser::stream_scanner::next_allowed_in_string() {
    return istream_ && (isalnum(istream_.peek()) || ispunct(istream_.peek())) && (!is_operator(istream_.peek()));
}

bool parser::stream_scanner::next_allowed_in_var_name() {
    return istream_ && (isalpha(istream_.peek()) || istream_.peek() == '_' || istream_.peek() == '?');
}

bool parser::stream_scanner::next_quote() {
    return istream_ && (istream_.peek() == QUOTE);
}

bool parser::stream_scanner::next_eof() {
    return istream_.eof();
}

bool parser::stream_scanner::next_whitespace() {
    return istream_ && isspace(istream_.peek());
}

bool parser::stream_scanner::next_variable_call_symbol() {
    return istream_ && (istream_.peek() == static_cast<char>(token_type::VAR_CALL_SYMBOL));
}

bool parser::stream_scanner::next_variable_call_symbol_open() {
    return istream_ && (istream_.peek() == static_cast<char>(token_type::VAR_CALL_OPEN));
}

bool parser::stream_scanner::next_variable_call_symbol_close() {
    return istream_ && (istream_.peek() == static_cast<char>(token_type::VAR_CALL_CLOSE));
}

char parser::stream_scanner::get_char() const {
    return static_cast<char>(istream_.get());
}

void parser::stream_scanner::skip_char() {
    istream_.get();
}
