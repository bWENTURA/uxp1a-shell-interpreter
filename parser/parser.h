// Maja Jabłońska

#ifndef UXP1A_SHELL_INTERPRETER_PARSER_H
#define UXP1A_SHELL_INTERPRETER_PARSER_H

#include <deque>
#include <local_assignment.h>
#include <global_assignment.h>
#include <custom_command.h>
#include <echo_command.h>
#include <pwd_command.h>
#include <cd_command.h>
#include <unevaluated_argument.h>
#include <node.h>
#include "stream_scanner.h"
#include "redirect_input.h"
#include "redirect_output.h"
#include "pipe.h"

namespace parser {
    class parser {
    public:
        explicit parser(std::istream& istream_);
        explicit parser(std::unique_ptr<scanning_device> sc);

        void parse();

        bool parse_export_assignment();
        ast::command* parse_command();
        bool parse_assignment();

        bool parse_redirect_in();
        bool parse_redirect_out();
        bool parse_pipe();

        bool check_export_assignment();
        bool check_assignment();
        bool check_redirect_in();
        bool check_redirect_out();
        bool check_pipe();

        ast::node* get_node();

    private:
        ast::unevaluated_argument* parse_argument();
        std::vector<ast::unevaluated_argument> parse_arguments();

        const bool command_end();
        const bool can_conc_with_escaped_var();
        const bool can_conc_with_var();
        const bool can_conc_with_string();
        const std::unique_ptr<scanning_device> sc;
        std::deque<ast::node*> nodes;
    };
}

#endif //UXP1A_SHELL_INTERPRETER_PARSER_H
