#define BOOST_TEST_DYN_LINK
// Maja Jabłońska
#include "../parser.h"
#define BOOST_TEST_MODULE ParserIntegrationTests
#include <boost/test/unit_test.hpp>
#include <iostream>

#include <local_assignment.h>
#include <global_assignment.h>
#include <custom_command.h>

std::stringstream st;
parser::parser p(st);

BOOST_AUTO_TEST_SUITE(parser_integration_tests)

BOOST_AUTO_TEST_CASE(parse_variable_assignment) {
    // given
    st << "var=sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::local_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr); // Check if node has expected dynamic type
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_to_quoted_str) {
    // given
    st << "var=\'doggie doggie what now\'";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::local_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr); // Check if node has expected dynamic type
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_with_delimiter) {
    // given
    st << "var=sth;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::local_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr); // Check if node has expected dynamic type
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_to_variable_call) {
    // given
    st << "var=$sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::local_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr); // Check if node has expected dynamic type
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_to_variable_call_with_delimiter) {
    // given
    st << "var=$sth;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::local_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr); // Check if node has expected dynamic type
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_to_quoted_str_with_delimiter) {
    // given
    st << "var=\'doggie doggie what now\';";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::local_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr); // Check if node has expected dynamic type
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment) {
    // given
    st << "export var=sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_escaped_var) {
    // given
    st << "export var=sth${var}sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_string_conc_var) {
    // given
    st << "export var=sth$a";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_to_quoted_string) {
    // given
    st << "export var=\'doggie doggie what now\'";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_to_quoted_string_with_var) {
    // given
    st << "export var=\'doggie doggie what now\'$a";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_to_quoted_string_with_delimiter) {
    // given
    st << "export var=\'doggie doggie what now\';";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_to_quoted_string_with_arg) {
    // given
    st << "export var=\'doggie doggie what now\'$a;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_with_escaped_var) {
    // given
    st << "export var=hello${there}hello;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_to_variable_call) {
    // given
    st << "export var=$sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_to_variable_call_with_delimiter) {
    // given
    st << "export var=$sth;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::global_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_command) {
    // given
    st << "echo";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_command_with_delimiter) {
    // given
    st << "echo;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_args) {
    // given
    st << "echo something";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_args_and_var) {
    // given
    st << "echo something$a";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_args_esc_var) {
    // given
    st << "echo sth${var}sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_quoted_string_arg) {
    // given
    st << "echo \'something\'";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_quoted_string_arg_var) {
    // given
    st << "echo \'something\'$a";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_quoted_string_arg_with_delimiter) {
    // given
    st << "echo \'something\';";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_quoted_string_arg_with_delimiter_var) {
    // given
    st << "echo \'something\'$a;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_escaped_var_string_arg_with_delimiter) {
    // given
    st << "echo ${escaped}str;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_variable_call) {
    // given
    st << "echo $sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_many_variables_call) {
    // given
    st << "echo $sth$a$sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_echo_with_variable_call_with_delimiter) {
    // given
    st << "echo $sth;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::echo_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}


BOOST_AUTO_TEST_CASE(parse_pwd_command) {
    // given
    st << "pwd";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::pwd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_pwd_command_with_delimiter) {
    // given
    st << "pwd;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::pwd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_pwd_with_args) {
    // given
    st << "pwd something";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::pwd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_pwd_with_quoted_string_arg) {
    // given
    st << "pwd \'something\'";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::pwd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_pwd_with_quoted_string_arg_with_delimiter) {
    // given
    st << "pwd \'something\';";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::pwd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_pwd_with_variable_call) {
    // given
    st << "pwd $sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::pwd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_pwd_with_many_variables_call) {
    // given
    st << "pwd $sth$a$sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::pwd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_pwd_with_variable_call_with_delimiter) {
    // given
    st << "pwd $sth;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::pwd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_cd_command) {
    // given
    st << "cd";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::cd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_cd_command_with_delimiter) {
    // given
    st << "cd;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::cd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_cd_with_args) {
    // given
    st << "cd something";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::cd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_cd_with_quoted_string_arg) {
    // given
    st << "cd \'something\'";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::cd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_cd_with_quoted_string_arg_with_delimiter) {
    // given
    st << "cd \'something\';";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::cd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_cd_with_variable_call) {
    // given
    st << "cd $sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::cd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_cd_with_variable_call_with_delimiter) {
    // given
    st << "cd $sth;";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::cd_command*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_command_with_redirect_in) {
    // given
    st << "command < sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node_r_in = dynamic_cast<ast::redirect_input*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node_r_in, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_command_with_redirect_out) {
    // given
    st << "command > sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node_r_in = dynamic_cast<ast::redirect_output*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node_r_in, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_command_with_pipe) {
    // given
    st << "echo | sth";
    // when
    p.parse();
    auto node_ptr = p.get_node();
    auto cast_node_p = dynamic_cast<ast::pipenode*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node_p, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_many_commands) {
    // given
    st << "cd $sth; pwd; echo sth; var=5; export b=5";
    // when
    p.parse();
    auto cd_ptr = p.get_node();
    auto cd_cast_node = dynamic_cast<ast::cd_command*>(cd_ptr);
    auto pwd_ptr = p.get_node();
    auto pwd_cast_node = dynamic_cast<ast::pwd_command*>(pwd_ptr);
    auto echo_ptr = p.get_node();
    auto echo_cast_node = dynamic_cast<ast::echo_command*>(echo_ptr);
    auto assignment_ptr = p.get_node();
    auto assignment_cast_node = dynamic_cast<ast::local_assignment*>(assignment_ptr);
    auto export_ptr = p.get_node();
    auto export_cast_node = dynamic_cast<ast::global_assignment*>(export_ptr);
    // then
    BOOST_CHECK_NE(cd_cast_node, nullptr);
    BOOST_CHECK_NE(pwd_cast_node, nullptr);
    BOOST_CHECK_NE(echo_cast_node, nullptr);
    BOOST_CHECK_NE(assignment_cast_node, nullptr);
    BOOST_CHECK_NE(export_cast_node, nullptr);
     // Check if empty node queue
}

BOOST_AUTO_TEST_CASE(parse_command_with_many_pipes_and_redirects) {
    // given
    st << "echo | sth > sth$sth ";
    // when
        p.parse();
        auto node_ptr = p.get_node();
        auto cast_node_red = dynamic_cast<ast::redirect_output*>(node_ptr);
        // then
        BOOST_CHECK_NE(cast_node_red, nullptr);
         // Check if empty node queue
}

BOOST_AUTO_TEST_SUITE_END()