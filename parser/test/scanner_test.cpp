#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
// Maja Jabłońska
#include "../stream_scanner.h"
#define BOOST_TEST_MODULE ScannerTest
#include <iostream>
#include <boost/test/unit_test.hpp>

std::stringstream stream_;
parser::stream_scanner scan(stream_);

void clear_scanner() {
    scan.get(scan.token_queue_size());
}

BOOST_AUTO_TEST_SUITE(scanner_tests)

BOOST_AUTO_TEST_CASE(scan_ascii_letter) {
    // given
    clear_scanner();
    stream_ << "a";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "a");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(scan_many_ascii_letters) {
    // given
    clear_scanner();
    stream_ << "a b c";
    // when
    scan.scan();
    scan.get(1);
    scan.get(1);
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "c");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(scan_pipe) {
    // given
    clear_scanner();
    stream_ << "|";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "|");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::PIPE);
}

BOOST_AUTO_TEST_CASE(scan_assign) {
    // given
    clear_scanner();
    stream_ << "=";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "=");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::ASSIGN);
}

BOOST_AUTO_TEST_CASE(scan_redirect_in) {
    // given
    clear_scanner();
    stream_ << "<";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "<");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::REDIRECT_IN);
}

BOOST_AUTO_TEST_CASE(scan_redirect_out) {
    // given
    clear_scanner();
    stream_ << ">";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), ">");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::REDIRECT_OUT);
}

BOOST_AUTO_TEST_CASE(scan_delimiter) {
    // given
    clear_scanner();
    stream_ << ";";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), ";");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::DELIMITER);
}

BOOST_AUTO_TEST_CASE(scan_string) {
    // given
    clear_scanner();
    stream_ << "hello there doggie";
    // when
    scan.scan();
    scan.get(1);
    scan.get(1);
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "doggie");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(scan_quoted_string) {
    // given
    clear_scanner();
    stream_ << "\' hello. I contain many wei$rd s\\ings> :) \'";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), " hello. I contain many wei$rd s\\ings> :) ");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::QUOTED_STRING);
}

BOOST_AUTO_TEST_CASE(scan_cd_command) {
    // given
    clear_scanner();
    stream_ << "cd";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "cd");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::CD);
}

BOOST_AUTO_TEST_CASE(scan_echo_command) {
    // given
    clear_scanner();
    stream_ << "echo";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "echo");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::ECHO);
}

BOOST_AUTO_TEST_CASE(scan_pwd_command) {
    // given
    clear_scanner();
    stream_ << "pwd";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "pwd");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::PWD);
}

BOOST_AUTO_TEST_CASE(scan_export_command) {
    // given
    clear_scanner();
    stream_ << "export";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "export");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::EXPORT);
}

BOOST_AUTO_TEST_CASE(dont_scan_uppercase_cd_command) {
    // given
    clear_scanner();
    stream_ << "CD";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "CD");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(dont_scan_uppercase_echo_command) {
    // given
    clear_scanner();
    stream_ << "ECHO";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "ECHO");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(dont_scan_uppercase_pwd_command) {
    // given
    clear_scanner();
    stream_ << "PWD";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "PWD");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(dont_scan_uppercase_export_command) {
    // given
    clear_scanner();
    stream_ << "EXPORT";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "EXPORT");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(scan_scoped_variable) {
    // given
    clear_scanner();
    stream_ << "${hello there}";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "hello there");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::ESCAPED_VAR);
}

BOOST_AUTO_TEST_CASE(assignment) {
    // given
    clear_scanner();
    stream_ << "a=3";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.token_queue_size(), 3);

    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "a");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);

    BOOST_CHECK_EQUAL(scan.peek_token_value(1), "=");
    BOOST_CHECK(scan.peek_token_type(1) == parser::token_type::ASSIGN);

    BOOST_CHECK_EQUAL(scan.peek_token_value(2), "3");
    BOOST_CHECK(scan.peek_token_type(2) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(redirect) {
    // given
    clear_scanner();
    stream_ << "command >file.txt";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.token_queue_size(), 3);

    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "command");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);

    BOOST_CHECK_EQUAL(scan.peek_token_value(1), ">");
    BOOST_CHECK(scan.peek_token_type(1) == parser::token_type::REDIRECT_OUT);

    BOOST_CHECK_EQUAL(scan.peek_token_value(2), "file.txt");
    BOOST_CHECK(scan.peek_token_type(2) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(export_assignment) {
    // given
    clear_scanner();
    stream_ << "export a=3";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.token_queue_size(), 4);

    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "export");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::EXPORT);

    BOOST_CHECK_EQUAL(scan.peek_token_value(1), "a");
    BOOST_CHECK(scan.peek_token_type(1) == parser::token_type::STRING);

    BOOST_CHECK_EQUAL(scan.peek_token_value(2), "=");
    BOOST_CHECK(scan.peek_token_type(2) == parser::token_type::ASSIGN);

    BOOST_CHECK_EQUAL(scan.peek_token_value(3), "3");
    BOOST_CHECK(scan.peek_token_type(3) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(export_assignment_with_typo) {
    // given
    clear_scanner();
    stream_ << "exporta=3";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.token_queue_size(), 3);

    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "exporta");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);

    BOOST_CHECK_EQUAL(scan.peek_token_value(1), "=");
    BOOST_CHECK(scan.peek_token_type(1) == parser::token_type::ASSIGN);

    BOOST_CHECK_EQUAL(scan.peek_token_value(2), "3");
    BOOST_CHECK(scan.peek_token_type(2) == parser::token_type::STRING);
}

BOOST_AUTO_TEST_CASE(variable_call_in_instruction) {
    // given
    clear_scanner();
    stream_ << "a=$b";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.token_queue_size(), 3);

    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "a");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);

    BOOST_CHECK_EQUAL(scan.peek_token_value(1), "=");
    BOOST_CHECK(scan.peek_token_type(1) == parser::token_type::ASSIGN);

    BOOST_CHECK_EQUAL(scan.peek_token_value(2), "b");
    BOOST_CHECK(scan.peek_token_type(2) == parser::token_type::VAR);
}

BOOST_AUTO_TEST_CASE(variable_scoped_call_in_instruction) {
    // given
    clear_scanner();
    stream_ << "a=${doggie doggie}";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.token_queue_size(), 3);

    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "a");
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::STRING);

    BOOST_CHECK_EQUAL(scan.peek_token_value(1), "=");
    BOOST_CHECK(scan.peek_token_type(1) == parser::token_type::ASSIGN);

    BOOST_CHECK_EQUAL(scan.peek_token_value(2), "doggie doggie");
    BOOST_CHECK(scan.peek_token_type(2) == parser::token_type::ESCAPED_VAR);
}

BOOST_AUTO_TEST_CASE(variable_name_cannot_have_punctuation) {
    // given
    clear_scanner();
    stream_ << "$b.";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.token_queue_size(), 2);
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::VAR);
    BOOST_CHECK(scan.peek_token_type(1) == parser::token_type::STRING);

    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "b");
    BOOST_CHECK_EQUAL(scan.peek_token_value(1), ".");
}

BOOST_AUTO_TEST_CASE(variable_name_can_have_underscores) {
    // given
    clear_scanner();
    stream_ << "$b_";
    // when
    scan.scan();
    // then
    BOOST_CHECK_EQUAL(scan.token_queue_size(), 1);
    BOOST_CHECK(scan.peek_token_type(0) == parser::token_type::VAR);

    BOOST_CHECK_EQUAL(scan.peek_token_value(0), "b_");
}

BOOST_AUTO_TEST_CASE(peek_value_out_of_scope) {
    // given
    clear_scanner();
    stream_ << "a=$b";
    // when
    scan.scan();
    // then
    BOOST_CHECK_THROW(scan.peek_token_value(10), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(peek_value_negative) {
    // given
    clear_scanner();
    stream_ << "a=$b";
    // when
    scan.scan();
    // then
    BOOST_CHECK_THROW(scan.peek_token_value(-10), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(peek_type_out_of_scope) {
    // given
    clear_scanner();
    stream_ << "a=$b";
    // when
    scan.scan();
    // then
    BOOST_CHECK_THROW(scan.peek_token_type(10), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(peek_type_negative) {
    // given
    clear_scanner();
    stream_ << "a=$b";
    // when
    scan.scan();
    // then
    BOOST_CHECK_THROW(scan.peek_token_type(-10), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(get_tokens_out_of_scope) {
    // given
    clear_scanner();
    stream_ << "a=$b";
    // when
    scan.scan();
    // then
    BOOST_CHECK_THROW(scan.get(10), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(get_tokens_negative) {
    // given
    clear_scanner();
    stream_ << "a=$b";
    // when
    scan.scan();
    // then
    BOOST_CHECK_THROW(scan.get(-10), std::runtime_error);
}

BOOST_AUTO_TEST_SUITE_END()