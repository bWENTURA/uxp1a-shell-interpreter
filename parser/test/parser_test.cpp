#define BOOST_TEST_DYN_LINK
// Maja Jabłońska
#include "../parser.h"
#define BOOST_TEST_MODULE ParserTest
#include <boost/test/unit_test.hpp>
#include <fakeit.hpp>

#include <local_assignment.h>
#include <global_assignment.h>
#include <custom_command.h>

#include "../scanning_device.h"

fakeit::Mock<parser::scanning_device> scanner;
parser::parser p(std::unique_ptr<parser::scanning_device>(&scanner.get()));

BOOST_AUTO_TEST_SUITE(parser_test)

BOOST_AUTO_TEST_CASE(parse_variable_assignment) {
    // given
    scanner.Reset();
    fakeit::Fake(Method(scanner, get));
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("varname");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("something");
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(3);
    // when
    bool result = p.check_assignment();
    // then
    BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_quoted) {
    // given
    scanner.Reset();
    fakeit::Fake(Method(scanner, get));
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::QUOTED_STRING);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("varname");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("\'something quoted\'");
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(3);
    // when
    bool result = p.check_assignment();
    // then
    BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_escaped) {
    // given
    scanner.Reset();
    fakeit::Fake(Method(scanner, get));
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::ESCAPED_VAR);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("varname");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("something something");
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(3);
    // when
    bool result = p.check_assignment();
    // then
    BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_escaped_var) {
    // given
    scanner.Reset();
    fakeit::Fake(Method(scanner, get));
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::ESCAPED_VAR);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("varname");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("something something");
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(3);
    // when
    bool result = p.check_assignment();
    // then
    BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment) {
    // given
    scanner.Reset();
    fakeit::Fake(Method(scanner, get));
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::EXPORT);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(3)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("export");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("varname");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(3)).AlwaysReturn("something");
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(4);
    // when
    bool result = p.check_export_assignment();
    // then
    BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_quoted) {
    // given
    scanner.Reset();
    fakeit::Fake(Method(scanner, get));
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::EXPORT);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(3)).AlwaysReturn(parser::token_type::QUOTED_STRING);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("export");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("varname");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(3)).AlwaysReturn("\'something quoted\'");
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(4);
    // when
    bool result = p.check_export_assignment();
    // then
    BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_with_typo) {
    // given
    scanner.Reset();
    fakeit::Fake(Method(scanner, get));
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::EXPORT);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(3)).AlwaysReturn(parser::token_type::REDIRECT_OUT);
    fakeit::When(Method(scanner, peek_token_type).Using(4)).AlwaysReturn(parser::token_type::QUOTED_STRING);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("export");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("varname");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(3)).AlwaysReturn(">");
    fakeit::When(Method(scanner, peek_token_value).Using(4)).AlwaysReturn("\'something quoted\'");
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(5);
    // when
    bool result = p.check_export_assignment();
    // then
    BOOST_CHECK(!result);
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_with_other_things) {
    // given
    scanner.Reset();
    fakeit::Fake(Method(scanner, get));
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::EXPORT);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(3)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(4)).AlwaysReturn(parser::token_type::REDIRECT_OUT);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("export");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("varname");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(3)).AlwaysReturn("sth");
    fakeit::When(Method(scanner, peek_token_value).Using(4)).AlwaysReturn(">");
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(5);
    // when
    bool result = p.check_export_assignment();
    // then
    BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(parse_variable_assignment_when_empty_token_queue) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(0);
    // when
    bool result = p.check_assignment();
    // then
    BOOST_CHECK(!result);
}

BOOST_AUTO_TEST_CASE(parse_export_variable_assignment_when_empty_token_queue) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(0);
    // when
    bool result = p.check_export_assignment();
    // then
    BOOST_CHECK(!result);
}

BOOST_AUTO_TEST_CASE(parse_command_when_empty) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).AlwaysReturn(0);
    // when
    bool result = p.parse_command();
    // then
    BOOST_CHECK(!result);
}

BOOST_AUTO_TEST_CASE(parse_command) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).Return(2).Return(2).Return(2).Return(2)
                                                            .Return(1).Return(1).Return(1).Return(1)
                                                            .Return(0).Return(0).Return(0).Return(0);
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::EXPORT);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("export");
    fakeit::Fake(Method(scanner, get));
    // when
    auto result = p.parse_command();
    auto cast = dynamic_cast<ast::custom_command*>(result);
    // then
    BOOST_CHECK_NE(cast, nullptr);
}

BOOST_AUTO_TEST_CASE(parse_command_until_delimiter) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).Return(2).Return(2).Return(2).Return(2)
                                                            .Return(1).Return(1).Return(1).Return(1)
                                                            .Return(0).Return(0).Return(0).Return(0);
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::EXPORT);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::DELIMITER);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("export");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn(";");
    fakeit::Fake(Method(scanner, get));
    // when
        auto result = p.parse_command();
        auto cast = dynamic_cast<ast::custom_command*>(result);
        // then
    BOOST_CHECK_NE(cast, nullptr);
}

BOOST_AUTO_TEST_CASE(parse_assignment_result_true) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).Return(3).Return(2).Return(1).Return(0);
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("variable");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("something");
    fakeit::Fake(Method(scanner, get));
    // when
    bool result = p.parse_assignment();
    p.get_node();
    // then
    BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(parse_assignment_build_node) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).Return(3).Return(2).Return(1).Return(0);
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_type).Using(1)).AlwaysReturn(parser::token_type::ASSIGN);
    fakeit::When(Method(scanner, peek_token_type).Using(2)).AlwaysReturn(parser::token_type::STRING);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("variable");
    fakeit::When(Method(scanner, peek_token_value).Using(1)).AlwaysReturn("=");
    fakeit::When(Method(scanner, peek_token_value).Using(2)).AlwaysReturn("something");
    fakeit::Fake(Method(scanner, get));
    // when
    p.parse_assignment();
    auto node_ptr = p.get_node();
    auto cast_node = dynamic_cast<ast::local_assignment*>(node_ptr);
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
}

BOOST_AUTO_TEST_CASE(parse_command_build_node) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).Return(1).Return(0);
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::EXPORT);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("export");
    fakeit::Fake(Method(scanner, get));
    // when
    auto cast_node = dynamic_cast<ast::custom_command*>(p.parse_command());
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
}

BOOST_AUTO_TEST_CASE(parse_echo_command_build_node) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).Return(1).Return(0);
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::ECHO);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("echo");
    fakeit::Fake(Method(scanner, get));
    // when
    auto cast_node = dynamic_cast<ast::echo_command*>(p.parse_command());
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
}

BOOST_AUTO_TEST_CASE(parse_cd_command_build_node) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).Return(1).Return(0);
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::CD);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("cd");
    fakeit::Fake(Method(scanner, get));
    // when
    auto cast_node = dynamic_cast<ast::cd_command*>(p.parse_command());
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
}

BOOST_AUTO_TEST_CASE(parse_pwd_command_build_node) {
    // given
    scanner.Reset();
    fakeit::When(Method(scanner, token_queue_size)).Return(1).Return(0);
    fakeit::When(Method(scanner, peek_token_type).Using(0)).AlwaysReturn(parser::token_type::PWD);
    fakeit::When(Method(scanner, peek_token_value).Using(0)).AlwaysReturn("pwd");
    fakeit::Fake(Method(scanner, get));
    // when
    auto cast_node = dynamic_cast<ast::pwd_command*>(p.parse_command());
    // then
    BOOST_CHECK_NE(cast_node, nullptr);
}

BOOST_AUTO_TEST_SUITE_END()
